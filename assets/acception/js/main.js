		function readURL_docs(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $(input).parent().find('img').attr('src', e.target.result).fadeIn('slow');
	        }
	        reader.readAsDataURL(input.files[0]);
	   	 }
		}


		$(".docs_selector").change(function(){
	        readURL_docs(this);
	    });


		function readURL(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
	        }
	        reader.readAsDataURL(input.files[0]);
	   	 }
		}


		$("#wizard-picture").change(function(){
	        readURL(this);
	    });


		function readURL2(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#wizardPicturePreview2').attr('src', e.target.result).fadeIn('slow');
	        }
	        reader.readAsDataURL(input.files[0]);
	   	 }
		}


		$("#wizard-picture2").change(function(){
	        readURL2(this);
	    });




		$(document).ready(function() {
			$('#validateForm').bootstrapValidator({
				feedbackIcons: {
					valid: 'glyphicon glyphicon-ok',
					invalid: 'glyphicon glyphicon-remove',
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					familiya: {
						validators: {
							stringLength: {
								min: 5,
								message: 'Илтимос, 5 та белгидан кўпроқ киритинг'
							},
							notEmpty: {
								message: 'Илтимос, фамилиянгизни киритинг'
							}
						}
					},
					ism: {
						validators: {
							stringLength: {
								min: 5,
								message: 'Илтимос, 5 та белгидан кўпроқ киритинг'
							},
							notEmpty: {
								message: 'Илтимос, исмингизни киритинг'
							}
						}
					},
					otasi: {
						validators: {
							stringLength: {
								min: 5,
								message: 'Илтимос, 5 та белгидан кўпроқ киритинг'
							},
							notEmpty: {
								message: 'Илтимос, отангизни исмини киритинг'
							}
						}
					},
					jins: {
					validators: {
						notEmpty: {
								message: 'Илтимос, жинсни танланг'
							}
						}
					},
					millat: {
					validators: {
						notEmpty: {
								message: 'Илтимос, миллатингизни танланг'
							}
						}
					},
					ty: {
						validators: {
							notEmpty: {
								message: 'Илтимос, туғилган кунингизни киритинг'
							}
						}
					},
					tel: {
						validators: {
							stringLength: {
								min: 9,
								max: 9,
								message: 'Илтимос, фақат 9 та белги бўлсин'
							},
							numeric: {
								message: 'Илтимос, фақат рақам киритинг'
							},
							notEmpty: {
								message: 'Илтимос, телефон рақамингизни киритинг'
							}
						}
					},
					tj: {
						validators: {
							stringLength: {
								min: 15,
								message: 'Илтимос, 15 та белгидан кўпроқ киритинг'
							},
							notEmpty: {
								message: 'Илтимос, туғилган жойингизни киритинг'
							}
						}
					},
					manzil: {
						validators: {
							stringLength: {
								min: 30,
								max: 100,
								message: 'Илтимос, манзил 30 та белгидан 100 та белгигача бўлсин'
							},
							notEmpty: {
								message: 'Илтимос, манзилингизни киритинг'
							}
						}
					},
					talim: {
						validators: {
							notEmpty: {
								message: 'Илтимос, тугатган мактабингизни  танланг'
							}
						}
					},
					pasport: {
						validators: {
							stringLength: {
								min: 9,
								message: 'Илтимос, 9 та белгидан кўпроқ киритинг'
							},
							notEmpty: {
								message: 'Илтимос, паспорт рақам киритинг'
							}
						}
					},
					harbiy: {
						validators: {
							notEmpty: {
									message: 'Илтимос,  имтиёзни танланг'
							}
						}
					},
					yunalish: {
					validators: {
						notEmpty: {
								message: 'Илтимос, йўналиш танланг'
							}
						}
					},
				}
			});
		});


		$(document).ready(function() {
			$('#validateForm2').bootstrapValidator({
				feedbackIcons: {
					valid: 'glyphicon glyphicon-ok',
					invalid: 'glyphicon glyphicon-remove',
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					familiya2: {
						validators: {
							stringLength: {
								min: 5,
								message: 'Илтимос, 5 та белгидан кўпроқ киритинг'
							},
							notEmpty: {
								message: 'Илтимос, фамилиянгизни киритинг'
							}
						}
					},
					ism2: {
						validators: {
							stringLength: {
								min: 5,
								message: 'Илтимос, 5 та белгидан кўпроқ киритинг'
							},
							notEmpty: {
								message: 'Илтимос, исмингизни киритинг'
							}
						}
					},
					otasi2: {
						validators: {
							stringLength: {
								min: 5,
								message: 'Илтимос, 5 та белгидан кўпроқ киритинг'
							},
							notEmpty: {
								message: 'Илтимос, отангизни исмини киритинг'
							}
						}
					},
					jins2: {
					validators: {
						notEmpty: {
								message: 'Илтимос, жинсни танланг'
							}
						}
					},
					millat2: {
					validators: {
						notEmpty: {
								message: 'Илтимос, миллатингизни танланг'
							}
						}
					},
					ty2: {
						validators: {
							notEmpty: {
								message: 'Илтимос, туғилган кунингизни киритинг'
							}
						}
					},
					tel2: {
						validators: {
							stringLength: {
								min: 9,
								max: 9,
								message: 'Илтимос, фақат 9 та белги бўлсин'
							},
							numeric: {
								message: 'Илтимос, фақат рақам киритинг'
							},
							notEmpty: {
								message: 'Илтимос, телефон рақамингизни киритинг'
							}
						}
					},
					tj2: {
						validators: {
							stringLength: {
								min: 15,
								message: 'Илтимос, 15 та белгидан кўпроқ киритинг'
							},
							notEmpty: {
								message: 'Илтимос, туғилган жойингизни киритинг'
							}
						}
					},
					manzil2: {
						validators: {
							stringLength: {
								min: 30,
								max: 100,
								message: 'Илтимос, манзил 30 та белгидан 100 та белгигача бўлсин'
							},
							notEmpty: {
								message: 'Илтимос, манзилингизни киритинг'
							}
						}
					},
					talim2: {
						validators: {
							notEmpty: {
								message: 'Илтимос, тугатган мактабингизни  танланг'
							}
						}
					},
					pasport2: {
						validators: {
							stringLength: {
								min: 9,
								message: 'Илтимос, 9 та белгидан кўпроқ киритинг'
							},
							notEmpty: {
								message: 'Илтимос, паспорт рақам киритинг'
							}
						}
					},
					harbiy2: {
						validators: {
							notEmpty: {
									message: 'Илтимос,  имтиёзни танланг'
							}
						}
					},
					yunalish2: {
					validators: {
						notEmpty: {
								message: 'Илтимос, йўналиш танланг'
							}
						}
					},
				}
			});
		});
		
		